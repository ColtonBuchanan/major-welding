<?php

/*
* CREATE TIME CARD CUSTOM POST TYPE
*/

function time_card_post_type() {

	// Set UI labels for time card
	$labels = array(
		'name'                => _x( 'Time Cards', 'Post Type General Name' ),
		'singular_name'       => _x( 'Time Card', 'Post Type Singular Name' ),
		'menu_name'           => __( 'Time Cards' ),
		'parent_item_colon'   => __( 'Parent Time Card' ),
		'all_items'           => __( 'All Time Cards' ),
		'view_item'           => __( 'View Time Card' ),
		'add_new_item'        => __( 'Add New Time Card' ),
		'add_new'             => __( 'Add New' ),
		'edit_item'           => __( 'Edit Time Card' ),
		'update_item'         => __( 'Update Time Card' ),
		'search_items'        => __( 'Search Time Card' ),
		'not_found'           => __( 'Not Found' ),
		'not_found_in_trash'  => __( 'Not Found in Trash' ),
	);
	
	// Set other options for time card
	$args = array(
		'label'               => __( 'Time Cards' ),
		'description'         => __( 'Your Time Cards' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'author', 'revisions', 'custom-fields' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'rewrite'             => array( 'slug' => 'time-cards' ),
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	
	// Register our time card post type
	register_post_type( 'time-card', $args );

}

add_action( 'init', 'time_card_post_type' );



/*
* MODIFY THE TIMECARD ARCHIVE TO ONLY SHOW TIMECARDS THAT BELONG TO THE CURRENT USER
*/

// function ls_modify_timecard_query($query) {

// 	if( $query->is_main_query() && is_post_type_archive('time-card') && ! is_admin() ) {
// 		$query->set( 'author', get_current_user_ID() );
// 		$query->set( 'post_status', array('order-assigned', 'awaiting-signature', 'publish') );
// 		return;
// 	}
// }

// add_action( 'pre_get_posts', 'ls_modify_timecard_query' );



/*
* AUTOPOPULATE DISPATCH FORM FIELDS WITH CUSTOMER'S COMPANY NAME, PHONE NUMBER, COMPANY ADDRESS, EMAIL, POSITION, AND THEIR PAST ORDERS
*/

add_filter( 'caldera_forms_render_get_field', function( $field )  {

	$user_id = get_current_user_ID();

	switch ( $field['slug'] ) {

		case 'company_name': 

			$field['config']['default'] = get_the_author_meta('ls_business_name', $user_id );
			break;

		case 'phone':

			$field['config']['default'] = get_the_author_meta('ls_phone_number', $user_id );
			break;

		case 'billing_address': 

			$field['config']['default'] = get_the_author_meta('ls_billing_address', $user_id );
			break;

		case 'email':

			$field['config']['default'] = get_the_author_meta('user_email', $user_id );
			break;

		case 'position':

			$field['config']['default'] = get_the_author_meta('position', $user_id );
			break;

		case 'which_order':

			// Query for previous orders and show them in a dropdown

			$args = array(
	    		'post_type'   => 'time-card',
	    		'post_status' => 'order-complete',
	    		'meta_key'    => 'customer_id',
	    		'meta_value'  => $user_id
	    	);

	    	$orders = new WP_Query($args);

	    	if ( $orders->have_posts() ) :

	    		while ( $orders->have_posts() ) : $orders->the_post();

	    			$name = get_the_title();
	    			$id   = get_the_ID();

	    			$field['config']['option'][] = array(
	    				'value' => $id, 
	    				'label' => $name,
	    			);

	    		endwhile;

	    		wp_reset_postdata();
	    		
	    	endif;

	    	break;


	}
 
    return $field;
 
});



/*
* CREATE TIME CARDS UPON DISPATCH FORM SUBMISSION
*/



function ls_create_timecard_data($data) {

	$current_user = wp_get_current_user();


	// If it's a renewal, do the following:

	if ( $data['is_a_renewal'] == 'yes' ) {


		// Identify the order that's getting renewed

		$original_name = $data['which_order'];


		// Grab all the data from that order

		$original_post = get_page_by_title( $original_name, OBJECT, 'time-card' );

		$original_id   = $original_post->ID;

		$original_data = array(
			'company',
			'customer_id',
			'customer_name',
			'customers_job_title',
			'customer_email',
			'customer_phone_number',
			'site_address',
			'site_contact_name',
			'site_contact_phone_number',
			'customer_po',
			'process',
			'buzz_box_required',
			'type_of_welding',
			'governing_code',
			'inspection_types',
			'material_type',
			'cable_required',
			'rig_welders_required',
		);


		// Create a new timecard 

		$args = array(

			'post_type'    => 'time-card',
			'post_title'   => '[RENEWED] ' . $original_post->post_title,
			'post_content' => '',
			'post_status'  => 'order-received',
			'post_author'  => 'Unassigned'
		);

		$new_time_card = wp_insert_post($args);


		// Add new time/date data from the form to the timecard

		update_post_meta($new_time_card, 'start_date', $data['start_date']);
		update_post_meta($new_time_card, 'start_time', $data['start_time']);
		update_post_meta($new_time_card, 'end_date', $data['end_date']);
		update_post_meta($new_time_card, 'end_time', $data['end_time']);


		// Also create an Event ID for Google Calendar

		update_post_meta($new_time_card, 'event_id', uniqid());


		// Transfer the data from the original timecard to the new one

		foreach ( $original_data as $field ) {

			$value = get_field($field, $original_id);

			update_post_meta($new_time_card, $field, $value);

		}


	// If it's not a renewal, create a brand new time card with data from the form

	} else {

		// Create a timecard with the data submitted through Caldera Forms

		$company = $data['company_name'];
		$address = $data['site_address'];

		$timecard_info = array(

			'post_type'    => 'time-card',
			'post_title'   => $company . ' | ' . $address,
			'post_status'  => 'order-received',
			'post_content' => '',
			'post_author'  => 'Unassigned',
		);

		$new_time_card = wp_insert_post($timecard_info);


		// Now let's update the simpler data fields

		$simple_data = array(

			'company'                   => $company,
			'billing_address'           => $data['billing_address'],
			'customer_id'               => get_current_user_ID(),
			'customer_name'             => $data['first_name'] . ' ' . $data['last_name'],
			'customers_job_title'       => $data['position'],
			'customer_email'            => $data['email'],
			'customer_phone_number'     => $data['phone'],
			'site_address'              => $address,
			'site_contact_name'         => $data['site_contact_name'],
			'site_contact_phone_number' => $data['site_contact_phone_number'],
			'start_date'                => $data['start_date'],
			'end_date'                  => $data['end_date'],
			'customer_po'               => $data['po'],
			'process'                   => $data['process'],
			'buzz_box_required'         => $data['buzz_box_required'],
			'inspection_types'	        => $data['inspection_types'] . ' - ' . $data['inspection_type_percentage']
		);

		foreach ( $simple_data as $field => $value ) {

			update_post_meta($new_time_card, $field, $value);

		}

		// Update welding type and governing code

		$welding_type = $data['type_of_welding'];

		if ( $welding_type == 'Pressure Pipe' ) {

			$pressure_pipe_sizes = $data['pressure_pipe_size'];

			$comma_sep_pressure_pipe_sizes = implode(', ', $pressure_pipe_sizes);

			update_post_meta($new_time_card, 'type_of_welding', $welding_type . ': ' . $comma_sep_pressure_pipe_sizes);

			$governing_code = $data['governing_code_pipe'];

			if ( $governing_code == 'Other' ) {

				update_post_meta($new_time_card, 'governing_code', $governing_code . ': ' . $data['other_code_pipe']);

			} else {

				update_post_meta($new_time_card, 'governing_code', $governing_code);
			}

		} else if ( $welding_type == 'Structural' ) {

			update_post_meta($new_time_card, 'type_of_welding', $welding_type);

			$governing_code = $data['governing_code_structural'];

			if ( $governing_code == 'Other' ) {
				update_post_meta($new_time_card, 'governing_code', $governing_code . ': ' . $data['other_code_structural']);
			} else {
				update_post_meta($new_time_card, 'governing_code', $governing_code);
			}

		} else if ( $welding_type == 'Repair/Maintenance' ) {

			update_post_meta($new_time_card, 'type_of_welding', $welding_type);

			$governing_code = $data['governing_code_repair_maintenance'];

			if ( $governing_code == 'Other' ) {
				update_post_meta($new_time_card, 'governing_code', $governing_code . ': ' . $data['other_code_repair_maintenance']);
			} else {
				update_post_meta($new_time_card, 'governing_code', $governing_code);
			}

		} else if ( $welding_type == 'Other' ) {

			update_post_meta($new_time_card, 'type_of_welding', $welding_type . ': ' . $data['type_of_welding_other']);

		}


		// Update material type

		$material_type = $data['material_type'];

		if ( $material_type == 'Stainless Steel' ) {

			$grade = $data['stainless_steel_grade'];

			if ( $grade == 'Other' ) {

				update_post_meta($new_time_card, 'material_type', $material_type . ': ' . $data['stainless_steel_grade_other'] );

			} else {

				update_post_meta($new_time_card, 'material_type', $material_type . ': ' . $grade );

			}

		} else {

			update_post_meta($new_time_card, 'material_type', $material_type);

		}


		// Update cable required

		$lead_cable_required = $data['lead_cable_required'];

		if ( $lead_cable_required == 'Yes' ) {

			$amount = $data['more_lead_cable_needed'];

			if ( $amount == 'Other' ) {

				update_post_meta($new_time_card, 'lead_cable_required', '200\' plus extra: ' . $data['extra_lead_cable_needed']);

			} else {

				update_post_meta($new_time_card, 'lead_cable_required', '200\' plus extra: ' . $amount);
			}

		} else {

			update_post_meta($new_time_card, 'lead_cable_required', 'Less than 200\'');

		}

		$ground_cable_required = $data['ground_cable_required'];

		if ( $ground_cable_required == 'Yes' ) {

			$amount = $data['more_ground_cable_needed'];

			if ( $amount == 'Other' ) {

				update_post_meta($new_time_card, 'ground_cable_required', '200\' plus extra: ' . $data['extra_ground_cable_needed']);

			} else {

				update_post_meta($new_time_card, 'ground_cable_required', '200\' plus extra: ' . $amount);
			}

		} else {

			update_post_meta($new_time_card, 'ground_cable_required', 'Less than 200\'');

		}


		// Update certificates required

		$certificates_required = $data['certificates_required'];

		if ( $certificates_required == 'No' ) {

			update_post_meta($new_time_card, 'certificates_required', $certificates_required);

		} else {

			$list_of_certs = $data['certificates_required_specific'];

			$comma_sep_certificates = implode(', ', $list_of_certs);

			update_post_meta($new_time_card, 'safety_certificates_required', $comma_sep_certificates);

		}


		// Update rig welders required

		$rig_welders_required = $data['rig_welders_required'];

		if ( $rig_welders_required == 'Other' ) {

			update_post_meta($new_time_card, 'rig_welders_required', $data['rig_welders_required_other']);

		} else {

			update_post_meta($new_time_card, 'rig_welders_required', $rig_welders_required);

		}


		// While we're at it, let's create an Event ID that we will later send to Google Calendar

		update_post_meta($new_time_card, 'event_id', uniqid());

	}
}

// Use the ls_submit_timecard hook we created in Caldera Forms to execute the function upon form submission
add_action( 'ls_submit_dispatch', 'ls_create_timecard_data');


/*
* POPULATE ACF STATUS FIELD WITH CURRENT POST STATUS IN FRONT-END
*/

// function ls_proper_time_card_status() {

// 	if( is_singular('time-card') && ! is_admin() ) {

// 		$status = get_post_status();
    
// 	    if ( $status       == 'order-received' ) {
// 	        update_field( 'status', 'Order Received', get_the_ID() );
// 	    } elseif ( $status == 'order-assigned' ) {
// 	        update_field( 'status', 'Order Assigned', get_the_ID() );
// 	    } elseif ( $status == 'awaiting-signature' ) {
// 	        update_field( 'status', 'Awaiting Signature', get_the_ID() );
// 	    } elseif ( $status  == 'order-complete' ) {
// 	        update_field( 'status', 'Order Complete', get_the_ID() );
// 	    }
// 	}

// }

// add_action('wp', 'ls_proper_time_card_status');


/*
* ALLOW THE TIME CARD POST STATUS TO BE CHANGED VIA THE ACF FORM
*/


// function ls_pre_save_post( $post_id ) {

//     $value = $_POST['fields']['field_56467ddbb2d52'];
    
//     if ( $value == 'Order Received' ) {
//         wp_update_post( array( 'ID' => $post_id, 'post_status' => 'order-received') );
//     } elseif ( $value == 'Order Assigned' ) {
//         wp_update_post( array( 'ID' => $post_id, 'post_status' => 'order-assigned') );
//     } elseif ( $value == 'Awaiting Signature' ) {
//         wp_update_post( array( 'ID' => $post_id, 'post_status' => 'awaiting-signature') );
//     } elseif ( $value == 'Order Complete' ) {
//         wp_update_post( array( 'ID' => $post_id, 'post_status' => 'order-complete') );
//     }

//     // Also update the ACF status field
//     update_field( '_status', $value, $post_id);
    

//     return $post_id;

// }

// add_filter('acf/pre_save_post' , 'ls_pre_save_post', 10, 1 );



// Update the order when changes digital signature is obtained and when "Is Work Completed" checkbox is ticked

function ls_update_order( $post_id ) {

	if ( is_admin() ) {
		return;
	}

    $digital_signature = $_POST['fields']['field_56c5168c493e8'];
    $is_work_completed = $_POST['fields']['field_561ebce5c99fc'];
    
    if ( ! empty( $digital_signature ) && $is_work_completed == true ) {

    	wp_update_post( array( 'ID' => $post_id, 'post_status' => 'order-complete') );

    } else if ( $is_work_completed == true ) {

    	wp_update_post( array( 'ID' => $post_id, 'post_status' => 'awaiting-signature') );

    }

}

add_filter('acf/save_post' , 'ls_update_order', 10, 1 );



/*
* MODIFY DEFAULT AUTHOR DROPDOWN TO ONLY INCLUDE WELDERS
*/

function ls_authors_dropdown($output) {

	// Cancel the function if the user is not editing a time card

	$current_screen = get_current_screen();

	if ( $current_screen->parent_file != 'edit.php?post_type=time-card' ) {
		return $output;
	}

	// Retrieve the usernames and ID's of all welders

	$welders = get_users( array(
		'role' => 'welder',
		'fields' => array('ID', 'user_login'),
	));

	// Retrieve the ID of the currently selected welder

	global $post;

	$current_welder = $post->post_author;

	// Create the actual dropdown box

	$output = '<select name="post_author_override" id="post_author_override" class="">';

	$selected = '';

	// Create an "unassigned" option

	if( 'Unassigned' == get_the_author() ) {
		$selected = ' selected="selected"';
	}

	$output .= '<option value="Unassigned"' . $selected . '>Unassigned</option>';

	// Create an option for each welder

	foreach($welders as $welder) {

		$selected = '';

		if( $welder->ID == $current_welder ) {
			$selected = ' selected="selected"';
		}

		$output .= '<option value="' . $welder->ID . '"' . $selected . '>' . $welder->user_login . '</option>';
	}

	$output .= '</select>';

	// Return the new dropdown menu

	return $output;

}

add_filter( 'wp_dropdown_users', 'ls_authors_dropdown');



/*
* DISPLAY PROPER TIME CARD (ACF) FORM DEPENDING ON USER TYPE
*/

function ls_display_acf_form() {

	acf_form(array(
		'post_id'	=> get_the_ID(),
		'submit_value' => 'Update'
	));

	if ( current_user_can('view_welder_pages' ) ) {
		?>

		<script>

			var form = jQuery('.acf-form');

			var toShow = [

				'#acf-start_time',
				'#acf-start_date',
				'#acf-end_time',
				'#acf-end_date',
				'#acf-work_description',
				'#acf-billable_hours',
				'#acf-is_work_completed',
				'#acf-digital_signature',
				'#acf-date_signed',
				'#poststuff > .field'

			];

			var selectors = toShow.join();

			form.find('.field').not(selectors).hide();
			form.find('.jSignature').hide();

		</script>

		<?php

	} else {

		?>

		<script>

		var form = jQuery('.acf-form');

		var toShow = [

			'#acf-digital_signature',
			'#acf-date_signed',
			'#poststuff > .field'

		];

		var selectors = toShow.join();

		form.find('.field').not(selectors).hide();

		</script>

		<?php

	}
}

/*
* DISPLAY THE REMAINING DATA DEPENDING ON USER TYPE
*/

function ls_display_order_details( $post_status ) {

	$ignore = array();

	// Determine if user is a welder or admin

	$welder = current_user_can('view_welder_pages');


	// Get all ACF data associated with the time card

	$fields = get_fields();

	
	// Create the table for outputting data

	if ( $fields ) {

		if ( $post_status !== 'order-complete' ) {

			echo '<a class="order-details-btn-link ' . $post_status . '" id="show_order_details" href="#">';

				echo '<div class="order-details-btn"><h3>Click for More Details <i class="fa fa-angle-down"></i></h3></div>';

			echo '</a>';

		}

		echo '<table class="ls-order-details ' . $post_status . '" id="details_to_show"><tbody>';
	}


	// Display the appropriate data

	foreach( $fields as $field_name => $value ) {

		$field = get_field_object($field_name, false, array('load_value' => false));
		
		if ( $welder ) {

			// If user is a welder/admin and order is complete, ignore items in array

			if ( $post_status == 'order-complete' ) {

				$ignore = array(
					'event_id',
					'end_date',
					'work_description',
					'billable_hours',
					'is_work_completed',
				);

			// If user is a welder/admin and order is not complete, ignore these other items in array

			} else {

				$ignore = array(
					'digital_signature',
					'date_signed',
					'event_id',
					'end_date',
					'work_description',
					'billable_hours',
					'is_work_completed',
				);
			}

			// Display the remaining items

			if ( in_array( $field_name, $ignore ) ) {

				continue;

			} else {

				if ( $field_name == 'digital_signature' ) {
					$value = '<img src="' . $value . '"/>';
				}

				echo '<tr><td>' . $field['label'] . '</td><td>' . $value . '</td></tr>';
			}

		} else {

			// If it's a customer, and the order is complete, ignore items in array

			if ( $post_status == 'order-complete' ) {

				$ignore = array(
					'event_id',
				);

			// If it's a customer, and the order hasn't been signed, ignore these other items in array

			} else {

				$ignore = array(
					'digital_signature',
					'date_signed',
					'event_id'
				);

			}	 

			// Display the remaining items

			if ( in_array( $field_name, $ignore ) ) {

				continue;
				
			} else {

				if ( $field_name == 'digital_signature' ) {
					$value = '<img src="' . $value . '"/>';
				}

				echo '<tr><td>' . $field['label'] . '</td><td>' . $value . '</td></tr>';
			}

		}

	}

	// Close the table (if there is one)

	if ( $fields ) {
		echo '</tbody></table>';
	}


}


?>