<?php

/*
* POPULATE ACF STATUS FIELD WITH CURRENT POST STATUS IN FRONT-END
*/

// function ls_proper_time_card_status() {

// 	if( is_singular('time-card') && ! is_admin() ) {

// 		$status = get_post_status();
    
// 	    if ( $status       == 'order-received' ) {
// 	        update_field( 'status', 'Order Received', get_the_ID() );
// 	    } elseif ( $status == 'order-assigned' ) {
// 	        update_field( 'status', 'Order Assigned', get_the_ID() );
// 	    } elseif ( $status == 'awaiting-signature' ) {
// 	        update_field( 'status', 'Awaiting Signature', get_the_ID() );
// 	    } elseif ( $status  == 'order-complete' ) {
// 	        update_field( 'status', 'Order Complete', get_the_ID() );
// 	    }
// 	}

// }

// add_action('wp', 'ls_proper_time_card_status');


/*
* ALLOW THE TIME CARD POST STATUS TO BE CHANGED VIA THE ACF FORM
*/


// function ls_pre_save_post( $post_id ) {

//     $value = $_POST['fields']['field_56467ddbb2d52'];
    
//     if ( $value == 'Order Received' ) {
//         wp_update_post( array( 'ID' => $post_id, 'post_status' => 'order-received') );
//     } elseif ( $value == 'Order Assigned' ) {
//         wp_update_post( array( 'ID' => $post_id, 'post_status' => 'order-assigned') );
//     } elseif ( $value == 'Awaiting Signature' ) {
//         wp_update_post( array( 'ID' => $post_id, 'post_status' => 'awaiting-signature') );
//     } elseif ( $value == 'Order Complete' ) {
//         wp_update_post( array( 'ID' => $post_id, 'post_status' => 'order-complete') );
//     }

//     // Also update the ACF status field
//     update_field( '_status', $value, $post_id);
    

//     return $post_id;

// }

// add_filter('acf/pre_save_post' , 'ls_pre_save_post', 10, 1 );



// Update the order when changes digital signature is obtained and when "Is Work Completed" checkbox is ticked

function ls_update_order( $post_id ) {

	if ( is_admin() ) {
		return;
	}

    $digital_signature = $_POST['fields']['field_56c5168c493e8'];
    $is_work_completed = $_POST['fields']['field_561ebce5c99fc'];
    
    if ( ! empty( $digital_signature ) && $is_work_completed == true ) {

    	wp_update_post( array( 'ID' => $post_id, 'post_status' => 'order-complete') );

    } else if ( $is_work_completed == true ) {

    	wp_update_post( array( 'ID' => $post_id, 'post_status' => 'awaiting-signature') );

    }

}

add_filter('acf/save_post' , 'ls_update_order', 10, 1 );



/*
* DISPLAY PROPER TIME CARD (ACF) FORM DEPENDING ON USER TYPE
*/

function ls_display_acf_form() {

	acf_form(array(
		'post_id'	=> get_the_ID(),
		'submit_value' => 'Update'
	));

	if ( current_user_can('view_welder_pages' ) ) {
		?>

		<script>

			var form = jQuery('.acf-form');

			var toShow = [

				'#acf-start_time',
				'#acf-start_date',
				'#acf-end_time',
				'#acf-end_date',
				'#acf-work_description',
				'#acf-billable_hours',
				'#acf-is_work_completed',
				'#acf-digital_signature',
				'#acf-date_signed',
				'#poststuff > .field'

			];

			var selectors = toShow.join();

			form.find('.field').not(selectors).hide();
			form.find('.jSignature').hide();

		</script>

		<?php

	} else {

		?>

		<script>

		var form = jQuery('.acf-form');

		var toShow = [

			'#acf-digital_signature',
			'#acf-date_signed',
			'#poststuff > .field'

		];

		var selectors = toShow.join();

		form.find('.field').not(selectors).hide();

		</script>

		<?php

	}
}

?>