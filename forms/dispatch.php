<?php

/*
* ADD INLINE JAVASCRIPT FOR DISPATCH FORM
*/

function ls_add_dispatch_form_js() {

	if ( is_page(219) ) {

		// If the current user is a welder, only allow them to renew existing orders

		if ( current_user_can('view_welder_pages') && ! current_user_can('create_users') ) {

		?>
		<script>
			jQuery('#fld_387273_1_opt1963067, #fld_387273_1_opt1596937').prop('disabled', true);
		</script>
		<?php

		}
	}
}

add_action('wp_footer', 'ls_add_dispatch_form_js');


/*
* AUTOPOPULATE DISPATCH FORM FIELDS WITH CUSTOMER'S COMPANY NAME, PHONE NUMBER, COMPANY ADDRESS, EMAIL, POSITION, AND THEIR PAST ORDERS
*/

add_filter( 'caldera_forms_render_get_field', function( $field )  {

	$user_id = get_current_user_ID();

	switch ( $field['slug'] ) {

		case 'is_a_renewal':

			if ( current_user_can('view_welder_pages') ) {
				$field['config']['default'] = 'opt1963067';
			}

			break;

		case 'company_name': 

			$field['config']['default'] = get_the_author_meta('ls_business_name', $user_id );
			break;

		case 'phone':

			$field['config']['default'] = get_the_author_meta('ls_phone_number', $user_id );
			break;

		case 'billing_address': 

			$field['config']['default'] = get_the_author_meta('ls_billing_address', $user_id );
			break;

		case 'email':

			$field['config']['default'] = get_the_author_meta('user_email', $user_id );
			break;

		case 'position':

			$field['config']['default'] = get_the_author_meta('position', $user_id );
			break;

		case 'which_order':

			// If the current user is a welder, query for open orders the welder is working on

			if ( current_user_can('view_welder_pages') && ! current_user_can('create_users') ) {

				$args = array(
		    		'post_type'   => 'time-card',
		    		'post_status' => 'order-assigned',
		    		'author'      => get_current_user_ID()
				);

			} else {

			// If the current user is a customer, query for open orders made by that customer

				$args = array(
		    		'post_type'   => 'time-card',
		    		'post_status' => 'order-assigned',
		    		'meta_key'    => 'customer_id',
		    		'meta_value'  => $user_id
		    	);
			}

	    	$orders = new WP_Query($args);

	    	if ( $orders->have_posts() ) :

	    		while ( $orders->have_posts() ) : $orders->the_post();

	    			$name = get_the_title();
	    			$id   = get_the_ID();

	    			$field['config']['option'][] = array(
	    				'value' => $id, 
	    				'label' => $name,
	    			);

	    		endwhile;

	    		wp_reset_postdata();
	    		
	    	endif;

	    	break;


	}
 
    return $field;
 
});



/*
* CREATE TIME CARDS UPON DISPATCH FORM SUBMISSION
*/



function ls_create_dispatch_data($data) {

	$current_user = wp_get_current_user();


	// If it's a renewal, do the following:

	if ( $data['is_a_renewal'] == 'yes' ) {


		// Identify the order that's getting renewed

		$original_name = $data['which_order'];


		// Grab all the data from that order

		$original_post = get_page_by_title( $original_name, OBJECT, 'time-card' );

		$original_id   = $original_post->ID;

		$original_data = array(
			'work_order_number',
			'company',
			'customer_id',
			'customer_name',
			'customers_job_title',
			'customer_email',
			'customer_phone_number',
			'site_address',
			'site_contact_name',
			'site_contact_phone_number',
			'customer_po',
			'process',
			'buzz_box_required',
			'type_of_welding',
			'governing_code',
			'inspection_types',
			'material_type',
			'cable_required',
			'rig_welders_required',
		);


		// Create a new timecard 

		$args = array(

			'post_type'    => 'time-card',
			'post_title'   => '[RENEWED] ' . $original_post->post_title,
			'post_content' => '',
			'post_status'  => 'order-received',
			'post_author'  => 'Unassigned'
		);

		$new_time_card = wp_insert_post($args);


		// Add new time/date data from the form to the timecard

		update_post_meta($new_time_card, 'start_date', $data['start_date']);
		update_post_meta($new_time_card, 'end_date', $data['end_date']);


		// Also create an Event ID for Google Calendar

		update_post_meta($new_time_card, 'event_id', uniqid());


		// Transfer the data from the original timecard to the new one

		foreach ( $original_data as $field ) {

			$value = get_field($field, $original_id);

			update_post_meta($new_time_card, $field, $value);

		}


	// If it's not a renewal, create a brand new time card with data from the form

	} else {

		// Create a timecard with the data submitted through Caldera Forms

		$work_order = ls_take_wo_number();
		$company = $data['company_name'];
		$address = $data['site_address'];

		$timecard_info = array(

			'post_type'    => 'time-card',
			'post_title'   => $work_order . ' | ' . $company . ' | ' . $address,
			'post_status'  => 'order-received',
			'post_content' => '',
			'post_author'  => 'Unassigned',
		);

		$new_time_card = wp_insert_post($timecard_info);


		// Now let's update the simpler data fields

		$simple_data = array(

			'work_order_number'         => $work_order,
			'company'                   => $company,
			'billing_address'           => $data['billing_address'],
			'customer_id'               => get_current_user_ID(),
			'customer_name'             => $data['first_name'] . ' ' . $data['last_name'],
			'customers_job_title'       => $data['position'],
			'customer_email'            => $data['email'],
			'customer_phone_number'     => $data['phone'],
			'site_address'              => $address,
			'site_contact_name'         => $data['site_contact_name'],
			'site_contact_phone_number' => $data['site_contact_phone_number'],
			'start_date'                => $data['start_date'],
			'end_date'                  => $data['end_date'],
			'customer_po'               => $data['po'],
			'process'                   => $data['process'],
			'buzz_box_required'         => $data['buzz_box_required']
		);

		foreach ( $simple_data as $field => $value ) {

			update_post_meta($new_time_card, $field, $value);

		}

		// Update welding type and governing code

		$welding_type = $data['type_of_welding'];

		if ( $welding_type == 'Pressure Pipe' ) {

			$pressure_pipe_sizes = $data['pressure_pipe_size'];

			$comma_sep_pressure_pipe_sizes = implode(', ', $pressure_pipe_sizes);

			update_post_meta($new_time_card, 'type_of_welding', $welding_type . ': ' . $comma_sep_pressure_pipe_sizes);

			$governing_code = $data['governing_code_pipe'];

			if ( $governing_code == 'Other' ) {

				update_post_meta($new_time_card, 'governing_code', $governing_code . ': ' . $data['other_code_pipe']);

			} else {

				update_post_meta($new_time_card, 'governing_code', $governing_code);
			}

		} else if ( $welding_type == 'Structural' ) {

			update_post_meta($new_time_card, 'type_of_welding', $welding_type);

			$governing_code = $data['governing_code_structural'];

			if ( $governing_code == 'Other' ) {
				update_post_meta($new_time_card, 'governing_code', $governing_code . ': ' . $data['other_code_structural']);
			} else {
				update_post_meta($new_time_card, 'governing_code', $governing_code);
			}

		} else if ( $welding_type == 'Repair/Maintenance' ) {

			update_post_meta($new_time_card, 'type_of_welding', $welding_type);

			$governing_code = $data['governing_code_repair_maintenance'];

			if ( $governing_code == 'Other' ) {
				update_post_meta($new_time_card, 'governing_code', $governing_code . ': ' . $data['other_code_repair_maintenance']);
			} else {
				update_post_meta($new_time_card, 'governing_code', $governing_code);
			}

		} else if ( $welding_type == 'Other' ) {

			update_post_meta($new_time_card, 'type_of_welding', $welding_type . ': ' . $data['type_of_welding_other']);

		}

		// Update inspection types

		$inspection_types = $data['inspection_types'];
		$inspection_types_output = '';

		if ( count($inspection_types) < 2 ) {

			$inspection_types_output = $inspection_type . ': ' . $percentage;

		} else {

			foreach ( $inspection_types as $inspection_type ) {

				$percentage = strtolower($inspection_type);
				$percentage = str_replace(' ', '_', $percentage);
				$percentage = $data[$percentage . '_inspection_percentage'];

				$inspection_types_output .= $inspection_type . ': ' . $percentage . '. ';
			}
		}

		update_post_meta($new_time_card, 'inspection_types', $inspection_types_output);		


		// Update material types

		$material_types = $data['material_type'];
		$comma_sep_materials = implode(', ', $material_types);

		if ( in_array('Stainless Steel', $material_types) ) {

			$grade = $data['stainless_steel_grade'];

			if ( $grade == 'Other' ) {
				$grade = $data['stainless_steel_grade_other'];
			}

			update_post_meta($new_time_card, 'material_type', $comma_sep_materials . '. Stainless Steel Grade: ' . $grade);

		} else {

			update_post_meta($new_time_card, 'material_type', $comma_sep_materials);
		}


		// Update cable required

		$lead_cable_required = $data['lead_cable_required'];

		if ( $lead_cable_required == 'Yes' ) {

			$amount = $data['more_lead_cable_needed'];

			if ( $amount == 'Other' ) {

				update_post_meta($new_time_card, 'lead_cable_required', '200\' plus extra: ' . $data['extra_lead_cable_needed']);

			} else {

				update_post_meta($new_time_card, 'lead_cable_required', '200\' plus extra: ' . $amount);
			}

		} else {

			update_post_meta($new_time_card, 'lead_cable_required', 'Less than 200\'');

		}

		$ground_cable_required = $data['ground_cable_required'];

		if ( $ground_cable_required == 'Yes' ) {

			$amount = $data['more_ground_cable_needed'];

			if ( $amount == 'Other' ) {

				update_post_meta($new_time_card, 'ground_cable_required', '200\' plus extra: ' . $data['extra_ground_cable_needed']);

			} else {

				update_post_meta($new_time_card, 'ground_cable_required', '200\' plus extra: ' . $amount);
			}

		} else {

			update_post_meta($new_time_card, 'ground_cable_required', 'Less than 200\'');

		}


		// Update certificates required

		$certificates_required = $data['certificates_required'];

		if ( $certificates_required == 'No' ) {

			update_post_meta($new_time_card, 'certificates_required', $certificates_required);

		} else {

			$list_of_certs = $data['certificates_required_specific'];

			$comma_sep_certificates = implode(', ', $list_of_certs);

			update_post_meta($new_time_card, 'safety_certificates_required', $comma_sep_certificates);

		}


		// Update rig welders required

		$rig_welders_required = $data['rig_welders_required'];

		if ( $rig_welders_required == 'Other' ) {

			update_post_meta($new_time_card, 'rig_welders_required', $data['rig_welders_required_other']);

		} else {

			update_post_meta($new_time_card, 'rig_welders_required', $rig_welders_required);

		}


		// While we're at it, let's create an Event ID that we will later send to Google Calendar

		update_post_meta($new_time_card, 'event_id', uniqid());

		// And let's send off the dispatch notification

		ls_submit_dispatch_notification($data, $new_time_card);

	}
}

// Use the ls_submit_dispatch hook we created in Caldera Forms to execute the function upon form submission
add_action( 'ls_submit_dispatch', 'ls_create_dispatch_data', 10 );





?>