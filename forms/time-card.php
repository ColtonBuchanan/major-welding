<?php

function ls_create_time_card_data($data) {

	$post_id      = intval($_GET['post_id']);
	$time_cards   = get_field('time_cards', $post_id);
	$current_user = wp_get_current_user();
	$welder_name  = $current_user->user_firstname . ' ' . $current_user->user_lastname;


	$time_cards[] = array(
		'date'                 => $data['date'],
		'welder'               => $welder_name,
		'work_description'     => $data['work_description'],
		'billable_hours'       => $data['billable_hours'],
		'customer_rep_on_site' => $data['customer_rep_on_site'],
		'site_contact'         => $data['site_contact'],
		'site_signature'       => $data['site_signature'], 
		'is_work_completed'    => $data['is_work_completed'],
		'return_date'          => $data['return_date'],
		'start_time'           => $data['start_time'],
		'end_time'             => $data['end_time']
	);

	update_field('time_cards', $time_cards, $post_id);


	// Send off the email notification

	ls_submit_time_card_notification($data, $post_id, $welder_name);

	// If Work is Completed, send a notification to MW Admin for that too

	// if ( $data['is_work_completed'] == 1 ) {

	// 	//Automatically update the order status

	// 		wp_update_post(array(
	// 			'ID' => $post_id, 
	// 			'post_status' => 'order-complete'
	// 		));

	// 	ls_work_completed_notification( $post_id );
	// }



}

add_action('ls_submit_time_card', 'ls_create_time_card_data', 10);


// function ls_complete_order($data) {

// 	$post_id = intval($_GET['post_id']);	

// 	if ( $data['is_work_completed'] ) {
// 		wp_update_post(array(
// 			'ID' => $post_id, 
// 			'post_status' => 'order-complete'
// 		));
// 	}

// }

// add_action('ls_submit_time_card', 'ls_complete_order', 20);


	// $time_cards = get_field('time_card', 1114);

	// echo '<pre>';
	// var_dump($time_cards);
	// echo '</pre>';
?>