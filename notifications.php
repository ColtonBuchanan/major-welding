<?php
/*
* This file controls email notifications sent out when various forms are submitted.
*/

// Get email for Major Welding admin

function ls_get_admin_email() {
	//return 'dispatch@majorwelding.com';
	return 'colton.buchanan@localsphere.com';
}

// Create notification on dispatch submission

function ls_submit_dispatch_notification($data, $post_id) {

	$admin_email       = ls_get_admin_email();
	$company           = $data['company_name'];
	$customer_id       = get_field( 'customer_id', $post_id ); //
	$customer_data     = get_user_meta( $customer_id); //
	$customer_email    = get_userdata($customer_id)->user_email; //
	$ap_email          = $customer_data['accounts_payable_email'][0]; //
	$order_name        = get_the_title( $post_id );
	$work_order        = get_field( 'work_order_number', $post_id );


	// Create default headers

	$headers  = 'From: Major Welding <noreply@majorwelding.com>' . "\r\n";
	$headers .= 'BCC: colton.buchanan@localsphere.com' . "\r\n";

	// Create the email subject
	$order_name = get_the_title( $post->ID );
	$subject    = '[DISPATCH] ' . 'New dispatch ' . $work_order . ' from ' . get_field('company', $post_id) . '.';

	// Create the email message
	$message = '<p style="text-align: center; background: black;"> <a href="http://www.majorwelding.localsphereprod.com/wp-content/uploads/2015/08/logo.png"><img class="alignnone size-medium wp-image-66" src="http://www.majorwelding.localsphereprod.com/wp-content/uploads/2015/08/logo2.png" alt="logo" width="300" height="79" /></a></p>

		<p>Please login to <a href="http://majorwelding.com/wp-admin">MajorWelding.com admin</a> to assign this project to a welder.</p>

		<h2 style="text-align: center; color: #dd4200;">DISPATCH REPORT</h2>
		
		<table style="width: 100%; border: 1px solid black;">
			<tbody>
				<tr style="border: 1px solid black; background-color: #dd4200; color: white;">
					<th style="text-align: center; border: 1px solid black;">Work Order #</th>
					<th style="text-align: center; border: 1px solid black;">Company</th>
					<th style="text-align: center; border: 1px solid black;">Customer Name</th>
					<th style="text-align: center; border: 1px solid black;">Customer\'s Position</th>
				</tr>
				<tr>
					<td style="text-align: center; border: 1px solid black;">' . $work_order . '</td>
					<td style="text-align: center; border: 1px solid black;">' . $company . '</td>
					<td style="text-align: center; border: 1px solid black;">' . $data['first_name'] . ' ' . $data['last_name'] . '</td>
					<td style="text-align: center; border: 1px solid black;">' . $data['position'] . '</td>
				</tr>
				<tr style="border: 1px solid black; background-color: #dd4200; color: white;">
					<th style="text-align: center; border: 1px solid black;">Customer PO#</th>
					<th style="text-align: center; border: 1px solid black;">Site Address</th>
					<th style="text-align: center; border: 1px solid black;">Billing  Address</th>
				</tr>
				<tr>
					<td style="text-align: center; border: 1px solid black;">' . $data['po'] . '</td>
					<td style="text-align: center; border: 1px solid black;">' . $data['site_address'] . '</td>
					<td style="text-align: center; border: 1px solid black;">' . $data['billing_address'] . '</td>
				</tr>
			</tbody>
		</table>
		&nbsp;
		<table style="width: 100%; border: 1px solid black;">
			<tbody>
				<tr style="border: 1px solid black; background-color: #dd4200; color: white;">
					<th style="text-align: center; border: 1px solid black;">Start Date</th>
					<th style="text-align: center; border: 1px solid black;">End Date</th>
				</tr>
			<tr>
				<td style="text-align: center; border: 1px solid black;">' . $data['start_date'] . ' ' . $data['last_name'] . '</td>
				<td style="text-align: center; border: 1px solid black;">' . $data['end_date'] . '</td>
			</tr>
			</tbody>
		</table>
		&nbsp;
		<table style="width: 100%; border: 1px solid black;">
			<tbody>
				<tr style="border: 1px solid black; background-color: #dd4200; color: white;">
					<th style="text-align: center; border: 1px solid black;">Type of Welding</th>
					<th style="text-align: center; border: 1px solid black;">Governing Code</th>
					<th style="text-align: center; border: 1px solid black;">Process</th>
					<th style="text-align: center; border: 1px solid black;">Safety Certificates Required</th>
				</tr>
			<tr>
				<td style="text-align: center; border: 1px solid black;">' . get_field('type_of_welding') . '</td>
				<td style="text-align: center; border: 1px solid black;">' . get_field('governing_code') . '</td>
				<td style="text-align: center; border: 1px solid black;">' . $data['process'] . '</td>
				<td style="text-align: center; border: 1px solid black;">' . get_field('safety_certificates_required') . '</td>
			</tr>
			</tbody>
		</table>
		&nbsp;
		<table style="width: 100%; border: 1px solid black;">
			<tbody>
				<tr style="border: 1px solid black; background-color: #dd4200; color: white;">
					<th style="text-align: center; border: 1px solid black;">Buzz Box Required</th>
					<th style="text-align: center; border: 1px solid black;">Inspection Types</th>
					<th style="text-align: center; border: 1px solid black;">Lead Cable Required</th>
					<th style="text-align: center; border: 1px solid black;">Ground Cable Required</th>
					<th style="text-align: center; border: 1px solid black;">Material Type</th>
					<th style="text-align: center; border: 1px solid black;">Rig Welders Required</th>
				</tr>
				<tr>
					<td style="text-align: center; border: 1px solid black;">' . $data['buzz_box_required'] . '</td>
					<td style="text-align: center; border: 1px solid black;">' . get_field('inspection_types', $post_id) . '</td>
					<td style="text-align: center; border: 1px solid black;">' . get_field('lead_cable_required', $post_id) . '</td>
					<td style="text-align: center; border: 1px solid black;">' . get_field('ground_cable_required', $post_id) . '</td>
					<td style="text-align: center; border: 1px solid black;">' . get_field('material_type', $post_id) . '</td>
					<td style="text-align: center; border: 1px solid black;">' . $data['rig_welders_required'] . '</td>
				</tr>
			</tbody>
		</table>
		&nbsp;
		<table style="width: 100%; border: 1px solid black;">
			<tbody>
				<tr style="border: 1px solid black; background-color: #dd4200; color: white;">
					<th style="text-align: center; border: 1px solid black;">Site Contact Name</th>
					<th style="text-align: center; border: 1px solid black;">Site Contact Phone Number</th>
				</tr>
				<tr>
					<td style="text-align: center; border: 1px solid black;">' . $data['site_contact_name'] . '</td>
					<td style="text-align: center; border: 1px solid black;">' . $data['site_contact_phone_number'] . '</td>
				</tr>
			</tbody>
		</table>';

	// Send this email in HTML format
	$headers .= 'Content-Type: text/html; charset=UTF-8' . "\r\n"; 

	
	// Send an email to the customer with the subject and message determined above
	wp_mail( ls_get_admin_email(), html_entity_decode($subject), $message, $headers );

}


// Create notification for welder assignment

function ls_welder_assignment_notification( $new_status, $old_status, $post ) {

	if ( $new_status != $old_status && $new_status === 'order-assigned' ) {

		$admin_email       = ls_get_admin_email();
		$welder            = $post->post_author;
		$welder_email      = get_the_author_meta( 'user_email', $welder );
		$work_order_number = get_field( 'work_order_number', $post->ID );
		$start_date        = get_field( 'start_date', $post->ID );
		$end_date          = get_field( 'end_date', $post->ID );

		// Create default headers

		$headers  = 'From: Major Welding <noreply@majorwelding.com>' . "\r\n";
		$headers .= 'Content-Type: text/html; charset=UTF-8' . "\r\n"; 
		$headers .= 'Reply-To: ' . $admin_email . "\r\n";

		// Create the email subject
		$subject = 'You\'ve been assigned job ' . $work_order_number . ' from ' . $start_date . ' to ' . $end_date;

		// Create the email message
		$message = '<p style="text-align: center; background: black;"> <a href="http://www.majorwelding.localsphereprod.com/wp-content/uploads/2015/08/logo.png"><img class="alignnone size-medium wp-image-66" src="http://www.majorwelding.localsphereprod.com/wp-content/uploads/2015/08/logo2.png" alt="logo" width="300" height="79" /></a></p>

		<p>Please reply to this message and confirm that you will be attending.</p>

		<h2 style="text-align: center; color: #dd4200;">DISPATCH REPORT</h2>
		
		<table style="width: 100%; border: 1px solid black;">
			<tbody>
				<tr style="border: 1px solid black; background-color: #dd4200; color: white;">
					<th style="text-align: center; border: 1px solid black;">Work Order #</th>
					<th style="text-align: center; border: 1px solid black;">Company</th>
					<th style="text-align: center; border: 1px solid black;">Customer Name</th>
					<th style="text-align: center; border: 1px solid black;">Customer\'s Position</th>
				</tr>
				<tr>
					<td style="text-align: center; border: 1px solid black;">' . $work_order . '</td>
					<td style="text-align: center; border: 1px solid black;">' . $company . '</td>
					<td style="text-align: center; border: 1px solid black;">' . get_field('customer_name', $post->ID) . '</td>
					<td style="text-align: center; border: 1px solid black;">' . get_field('position', $post->ID) . '</td>
				</tr>
				<tr style="border: 1px solid black; background-color: #dd4200; color: white;">
					<th style="text-align: center; border: 1px solid black;">Customer PO#</th>
					<th style="text-align: center; border: 1px solid black;">Site Address</th>
					<th style="text-align: center; border: 1px solid black;">Billing  Address</th>
				</tr>
				<tr>
					<td style="text-align: center; border: 1px solid black;">' . get_field('po', $post->ID) . '</td>
					<td style="text-align: center; border: 1px solid black;">' . get_field('site_address', $post->ID) . '</td>
					<td style="text-align: center; border: 1px solid black;">' . get_field('billing_address', $post->ID) . '</td>
				</tr>
			</tbody>
		</table>
		&nbsp;
		<table style="width: 100%; border: 1px solid black;">
			<tbody>
				<tr style="border: 1px solid black; background-color: #dd4200; color: white;">
					<th style="text-align: center; border: 1px solid black;">Start Date</th>
					<th style="text-align: center; border: 1px solid black;">End Date</th>
				</tr>
			<tr>
				<td style="text-align: center; border: 1px solid black;">' . $start_date . '</td>
				<td style="text-align: center; border: 1px solid black;">' . $end_date . '</td>
			</tr>
			</tbody>
		</table>
		&nbsp;
		<table style="width: 100%; border: 1px solid black;">
			<tbody>
				<tr style="border: 1px solid black; background-color: #dd4200; color: white;">
					<th style="text-align: center; border: 1px solid black;">Type of Welding</th>
					<th style="text-align: center; border: 1px solid black;">Governing Code</th>
					<th style="text-align: center; border: 1px solid black;">Process</th>
					<th style="text-align: center; border: 1px solid black;">Safety Certificates Required</th>
				</tr>
			<tr>
				<td style="text-align: center; border: 1px solid black;">' . get_field('type_of_welding', $post->ID) . '</td>
				<td style="text-align: center; border: 1px solid black;">' . get_field('governing_code', $post->ID) . '</td>
				<td style="text-align: center; border: 1px solid black;">' . get_field('process', $post->ID) . '</td>
				<td style="text-align: center; border: 1px solid black;">' . get_field('safety_certificates_required', $post->ID) . '</td>
			</tr>
			</tbody>
		</table>
		&nbsp;
		<table style="width: 100%; border: 1px solid black;">
			<tbody>
				<tr style="border: 1px solid black; background-color: #dd4200; color: white;">
					<th style="text-align: center; border: 1px solid black;">Buzz Box Required</th>
					<th style="text-align: center; border: 1px solid black;">Inspection Types</th>
					<th style="text-align: center; border: 1px solid black;">Lead Cable Required</th>
					<th style="text-align: center; border: 1px solid black;">Ground Cable Required</th>
					<th style="text-align: center; border: 1px solid black;">Material Type</th>
					<th style="text-align: center; border: 1px solid black;">Rig Welders Required</th>
				</tr>
				<tr>
					<td style="text-align: center; border: 1px solid black;">' . get_field('buzz_box_required', $post->ID) . '</td>
					<td style="text-align: center; border: 1px solid black;">' . get_field('inspection_types', $post->ID) . '</td>
					<td style="text-align: center; border: 1px solid black;">' . get_field('lead_cable_required', $post->ID) . '</td>
					<td style="text-align: center; border: 1px solid black;">' . get_field('ground_cable_required', $post->ID) . '</td>
					<td style="text-align: center; border: 1px solid black;">' . get_field('material_type', $post->ID) . '</td>
					<td style="text-align: center; border: 1px solid black;">' . get_field('rig_welders_required', $post->ID) . '</td>
				</tr>
			</tbody>
		</table>
		&nbsp;
		<table style="width: 100%; border: 1px solid black;">
			<tbody>
				<tr style="border: 1px solid black; background-color: #dd4200; color: white;">
					<th style="text-align: center; border: 1px solid black;">Site Contact Name</th>
					<th style="text-align: center; border: 1px solid black;">Site Contact Phone Number</th>
				</tr>
				<tr>
					<td style="text-align: center; border: 1px solid black;">' . get_field('site_contact_name', $post->ID) . '</td>
					<td style="text-align: center; border: 1px solid black;">' . get_field('site_contact_phone_number', $post->ID) . '</td>
				</tr>
			</tbody>
		</table>';
	
		// Send an email to the customer with the subject and message determined above
		wp_mail( $welder_email, html_entity_decode($subject), $message, $headers );

	}	
}

add_action( 'transition_post_status', 'ls_welder_assignment_notification', 10, 3 );



function ls_customer_assignment_notification( $new_status, $old_status, $post ) {

	if ( $new_status != $old_status && $new_status === 'order-assigned' ) {

		$admin_email       = ls_get_admin_email();
		$ap_email          = $customer_data['accounts_payable_email'][0];
		$work_order_number = get_field( 'work_order_number', $post->ID );
		$start_date        = get_field( 'start_date', $post->ID );
		$end_date          = get_field( 'end_date', $post->ID );
		$customer_id       = get_field( 'customer_id', $post_id ); //
		$customer_data     = get_user_meta( $customer_id ); //
		$customer_email    = get_userdata($customer_id)->user_email; //
		$welder            = get_user_by('id', $post->post_author);

		// Create default headers

		$headers  = 'From: Major Welding <noreply@majorwelding.com>' . "\r\n";
		$cc = array( $ap_email );
		$headers .= 'CC: ' . implode(', ', $cc) . "\r\n";
		$headers .= 'Content-Type: text/html; charset=UTF-8' . "\r\n"; 

		// Create the email subject
		$subject = 'A welder has been assigned to your job ' . $work_order_number . '.';

		// Create the email message
		$message = '<p style="text-align: center; background: black;"> <a href="http://www.majorwelding.localsphereprod.com/wp-content/uploads/2015/08/logo.png"><img class="alignnone size-medium wp-image-66" src="http://www.majorwelding.localsphereprod.com/wp-content/uploads/2015/08/logo2.png" alt="logo" width="300" height="79" /></a></p>

		<h2 style="text-align: center; color: #dd4200;">WELDER ASSIGNED</h2>
		
		<table style="width: 100%; border: 1px solid black;">
			<tbody>
				<tr style="border: 1px solid black; background-color: #dd4200; color: white;">
					<th style="text-align: center; border: 1px solid black;">Work Order #</th>
					<th style="text-align: center; border: 1px solid black;">Company</th>
					<th style="text-align: center; border: 1px solid black;">Welder</th>
					
				</tr>
				<tr>
					<td style="text-align: center; border: 1px solid black;">' . $work_order_number . '</td>
					<td style="text-align: center; border: 1px solid black;">' . get_field('company', $post->ID) . '</td>
					<td style="text-align: center; border: 1px solid black;">' . $welder->display_name . '</td>
				</tr>
				<tr style="border: 1px solid black; background-color: #dd4200; color: white;">
					<th style="text-align: center; border: 1px solid black;">Customer PO#</th>
					<th style="text-align: center; border: 1px solid black;">Site Address</th>
					<th style="text-align: center; border: 1px solid black;">Billing Address</th>
				</tr>
				<tr>
					<td style="text-align: center; border: 1px solid black;">' . get_field('customer_po', $post->ID) . '</td>
					<td style="text-align: center; border: 1px solid black;">' . get_field('site_address', $post->ID) . '</td>
					<td style="text-align: center; border: 1px solid black;">' . get_field('billing_address', $post->ID) . '</td>
				</tr>
			</tbody>
		</table>
		&nbsp;
		<table style="width: 100%; border: 1px solid black;">
			<tbody>
				<tr style="border: 1px solid black; background-color: #dd4200; color: white;">
					<th style="text-align: center; border: 1px solid black;">Start Date</th>
					<th style="text-align: center; border: 1px solid black;">End Date</th>
				</tr>
			<tr>
				<td style="text-align: center; border: 1px solid black;">' . get_field('start_date', $post->ID) . '
				<td style="text-align: center; border: 1px solid black;">' . get_field('end_date', $post->ID) . '</td>
			</tr>
			</tbody>
		</table>';
	
		// Send an email to the customer with the subject and message determined above
		wp_mail( $customer_email, html_entity_decode($subject), $message, $headers );

	}	
}

add_action( 'transition_post_status', 'ls_customer_assignment_notification', 20, 3 );



// Create notification on time card submission

function ls_submit_time_card_notification($data, $post_id, $welder_name) {

	$admin_email       = ls_get_admin_email();
	$customer_id       = get_field( 'customer_id', $post_id ); //
	$customer_data     = get_user_meta( $customer_id ); //
	$customer_email    = get_userdata($customer_id)->user_email; //
	$ap_email          = $customer_data['accounts_payable_email'][0]; //
	$welder_email      = get_the_author_meta( 'user_email', $post_id );
	$order_name        = get_the_title( $post_id );
	$work_order_number = get_field( 'work_order_number', $post_id );
	$date              = $data['date'];
	$time_stamp        = strtotime($date);

	// Convert signature to a PNG and upload it to the server

	$sig = $data['site_signature'];

	list($type, $sig) = explode(';', $sig);
	list(, $sig)      = explode(',', $sig);
	$sig = base64_decode($sig);

	file_put_contents('/nas/content/staging/majorwelding/wp-content/plugins/localsphere-custom-dev/signatures/' . $work_order_number . '-' . $time_stamp . '.png', $sig);


	// Convert "Is Work Completed" value to "Yes" or "No"

	if ( $data['is_work_completed'] == 1 ) {
		$is_work_completed = 'Yes';
	} else {
		$is_work_completed = 'No';
	}

	// Create default headers
	$cc = array( $ap_email, $welder_email, $admin_email );

	$headers  = 'From: Major Welding <dispatch@majorwelding.com>' . "\r\n";
	$headers .= 'CC: ' . implode(', ', $cc) . "\r\n";
	//$headers .= 'BCC: colton.buchanan@localsphere.com' . "\r\n";

	// Create the email subject
	$subject = 'Your customer service report for ' . $work_order_number . ' (' . $data['date'] . ')' . ' is ready.';

	// Create the email message
	$message = '

	<p style="text-align: center; background: black;"> <a href="http://www.majorwelding.localsphereprod.com/wp-content/uploads/2015/08/logo.png"><img class="alignnone size-medium wp-image-66" src="http://www.majorwelding.localsphereprod.com/wp-content/uploads/2015/08/logo2.png" alt="logo" width="300" height="79" /></a></p>

	<h2 style="text-align: center; color: #dd4200;">CUSTOMER SERVICE REPORT</h2>
	
	&nbsp;
	<table style="width: 100%; border: 1px solid black;">
		<tbody>
			<tr style="border: 1px solid black; background-color: #dd4200; color: white;">
				<th style="text-align: center; border: 1px solid black;">Work Order Number</th>
				<th style="text-align: center; border: 1px solid black;">Site Address</th>
				<th style="text-align: center; border: 1px solid black;">Date</th>
				<th style="text-align: center; border: 1px solid black;">Welder</th>
			</tr>
		<tr>
			<td style="text-align: center; border: 1px solid black;">' . $work_order_number . '</td>
			<td style="text-align: center; border: 1px solid black;">' . get_field('site_address', $post_id) . '</td>
			<td style="text-align: center; border: 1px solid black;">' . $data['date'] . '</td>
			<td style="text-align: center; border: 1px solid black;">' . $welder_name . '</td>
		</tr>
		</tbody>
	</table>
	&nbsp;
	<table style="width: 100%; border: 1px solid black;">
		<tbody>
			<tr style="border: 1px solid black; background-color: #dd4200; color: white;">
				<th style="text-align: center; border: 1px solid black;">Billable Hours</th>
				<th style="text-align: center; border: 1px solid black;">Work Description</th>
				<th style="text-align: center; border: 1px solid black;">Start Time</th>
				<th style="text-align: center; border: 1px solid black;">End Time</th>				
			</tr>
			<tr>
				<td style="text-align: center; border: 1px solid black;">' . $data['billable_hours'] . '</td>
				<td style="text-align: center; border: 1px solid black;">' . $data['work_description'] . '</td>
				<td style="text-align: center; border: 1px solid black;">' . $data['start_time'] . '</td>
				<td style="text-align: center; border: 1px solid black;">' . $data['end_time'] . '</td>
			</tr>
		</tbody>
	</table>	
	&nbsp;
	<table style="width: 100%; border: 1px solid black;">
		<tbody>
			<tr style="border: 1px solid black; background-color: #dd4200; color: white;">
				<th style="text-align: center; border: 1px solid black;">Is work completed?</th>
				<th style="text-align: center; border: 1px solid black;">Return Date</th>
			</tr>
			<tr>
				<td style="text-align: center; border: 1px solid black;">' . $is_work_completed . '</td>
				<td style="text-align: center; border: 1px solid black;">' . $data['return_date'] . '</td>
			</tr>
		</tbody>
	</table>
	<table style="width: 100%; border: 1px solid black;">
		<tbody>
			<tr style="border: 1px solid black; background-color: #dd4200; color: white;">
				<th style="text-align: center; border: 1px solid black;">Site Contact</th>
				<th style="text-align: center; border: 1px solid black;">Site Contact Signature</th>
			</tr>
			<tr>
				<td style="text-align: center; border: 1px solid black;">' . $data['site_contact'] . '</td>
				<td style="text-align: center; border: 1px solid black;">
					<img src="http://majorwelding.staging.wpengine.com/wp-content/plugins/localsphere-custom-dev/signatures/' . $work_order_number . '-' . $time_stamp . '.png' . '"/>
				</td>
			</tr>
		</tbody>
	</table>';


	// Send this email in HTML format
	$headers .= 'Content-Type: text/html; charset=UTF-8' . "\r\n"; 
	
	// Send an email to the customer with the subject and message determined above
	wp_mail( ls_get_admin_email(), html_entity_decode($subject), $message, $headers );

}

//add_action( 'ls_submit_time_card', 'ls_submit_time_card_notification', 20 );



// Create notification on "Work is Completed"

function ls_work_completed_notification( $post_id, $is_work_completed ) {

	$admin_email       = ls_get_admin_email();
	$work_order_number = get_field( 'work_order_number', $post_id );

	// Create default headers
	$cc = array( $admin_email );

	$headers  = 'From: Major Welding <dispatch@majorwelding.com>' . "\r\n";
	//$headers .= 'CC: ' . implode(', ', $cc) . "\r\n";
	//$headers .= 'BCC: colton.buchanan@localsphere.com' . "\r\n";
	$headers .= 'Content-Type: text/html; charset=UTF-8' . "\r\n"; 

	// Create the email subject
	$subject = 'Order #' . $work_order_number . ' has been completed. ' . $is_work_completed;

	// Create the email message
	$message = '<p style="text-align: center; background: black;"> <a href="http://www.majorwelding.localsphereprod.com/wp-content/uploads/2015/08/logo.png"><img class="alignnone size-medium wp-image-66" src="http://www.majorwelding.localsphereprod.com/wp-content/uploads/2015/08/logo2.png" alt="logo" width="300" height="79" /></a></p>

		<p>Work has been completed for Order Number #' . $work_order_number . '.</p>

		<p>You may now close the order by changing the order status to "Order Complete" in the <a href="http://majorwelding.com/wp-admin" target="_blank">MajorWelding.com dashboard.</a></p>';

	// Send an email to the customer with the subject and message determined above
	wp_mail( ls_get_admin_email(), html_entity_decode($subject), $message, $headers );	

}

?>