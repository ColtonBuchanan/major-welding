/*
* Calculate and output additional cable charges depending on selections in Dispatch form.
*/

jQuery(document).ready(function($) {

	function cableCharges(field, className, conditional = false) {

		// Whenever the value of the field changes, run this function

		$(document).on('change', field, function() {

			var outputSelector = '.' + className;

			// Parse the field for a number and round it up to the nearest multiple of 50

			var cableNeeded = Math.ceil( parseInt($(field).val()) / 50) * 50;

			// If the field is not a number, don't output anything and abandon function

			if (isNaN(cableNeeded)) {
				$(outputSelector).remove();
				return;
			}

			// Get the actual cost ($25 per 50 feet of cable)

			var cost = '$' + (25 * (cableNeeded / 50));

			// Remove cost output from conditional fields. We only want to output it in one place.

			if (conditional) {
				$(conditional).remove();
			}

			// Create the cable cost HTML if it hasn't been created already

			if ( $(outputSelector).length == 0 ) {

				$(field).after('<div class="cable-cost ' + className + '"><i class="fa fa-exclamation-circle"></i> This will cost an extra <span class="' + className + '_price"></span>.</div>');

			}

			// Output the correct price

			$(outputSelector + '_price').html(cost);
		});

	}

	cableCharges('#fld_1114433_1', 'lead-cable-cost', '');
	cableCharges('#fld_6110750_1', 'ground-cable-cost', '');
	cableCharges('#fld_8962792_1', 'lead-cable-cost-specified', '.lead-cable-cost');
	cableCharges('#fld_6360900_1', 'ground-cable-cost-specified', '.ground-cable-cost');

});