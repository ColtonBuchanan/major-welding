/*
* This file uses the jQuery plugin "jSignature". 
* Here we are adding electronic signature functionality to the front-end of our Time Cards. 
* The signature is then saved to the back-end as an image URL along with the rest of the Advanced Custom Fields form data.
*/


jQuery(document).ready(function($) {

	// Hide the digital signature text field. Its purpose is to hold the file path to the image once the signature is completed.
	// Customers don't need to see it.

	// Cache our selectors
	var form         = $('.CF5701f5a031879');
	var sigContainer = form.find('.site-signature-field');
	var sig          = sigContainer.find('#fld_8559386_1');
	var newSig       = sigContainer.find('.jSignature');

	function displaySignature() {

		sig.val('').hide();

		console.log(sigContainer);

		// If a signature already exists, show the signature as an image

		if ( sig.val() ) {

			console.log('Sig already exists');

			sigContainer.append('<img src="' + sig.val() + '"/>');
			newSig.hide();

		// Otherwise create the canvas for the digital signature.

		} else {

			console.log('SigContainer does not exist');

			if ( sigContainer.is(':visible') ) {

				console.log('SigContainer is visible');

				sigContainer.jSignature();

			}
		}

	}

	// Run displaySignature on page load and every time the conditional field above is changed

	displaySignature();

	$(document).on('change', '#fld_3251952_1_opt1649548', function() {
		$('.site-signature-field, .site-contact-field').hide();
	});

	$(document).on('change', '#fld_3251952_1_opt2091267', function() {
		$('.site-signature-field, .site-contact-field').show();
	});


	// When the Update button is clicked, do the following:

    $('input[type="submit"]').on('click', function() {

    	// Save the canvas as an image URL

    	var img = form.jSignature('getData');

    	// Add this URL to our hidden digital signature text field. Now this reference will be saved into the back-end.

    	sig.val(img);

    });

});