<?php

function ls_get_gcal_shortcode() {

	$user_id   = get_current_user_ID();
	$user_data = get_user_meta($user_id);
	$shortcode = $user_data['google_calendar_shortcode'][0];

	if ( !empty( $shortcode ) ) {

		return do_shortcode($shortcode);

	} else {

		return false;
	}

}

add_shortcode( 'display_welder_calendar', 'ls_get_gcal_shortcode' );



// CREATE SHORTCODES TO RETRIEVE USER INFO

// function ls_get_first_name() {

// 	$ls_current_user = wp_get_current_user();

// 	return $ls_current_user->user_firstname;
// }

// add_shortcode( 'ls_first_name', 'ls_get_first_name' );


// function ls_get_last_name() {

// 	$ls_current_user = wp_get_current_user();

// 	return $ls_current_user->user_lastname;
// }

// add_shortcode( 'ls_last_name', 'ls_get_last_name' );


// function ls_get_email() {

// 	$ls_current_user = wp_get_current_user();

// 	return $ls_current_user->user_email;
// }

// add_shortcode( 'ls_email', 'ls_get_email' );


// function ls_get_business_name() {

//     return get_the_author_meta( 'ls_business_name', get_current_user_ID() );
// }

// add_shortcode( 'ls_business_name', 'ls_get_business_name' );


// function ls_get_billing_address() {

// 	//return get_the_author_meta( 'billing_address', get_current_user_ID() );
//     return get_the_author_meta( 'ls_billing_address', get_current_user_ID() );
// }

// add_shortcode( 'ls_billing_address', 'ls_get_billing_address' );



// function ls_get_phone_number () {

//     //return get_the_author_meta( 'billing_address', get_current_user_ID() );
//     return get_the_author_meta( 'ls_phone_number', get_current_user_ID() );
// }

// add_shortcode( 'ls_phone_number', 'ls_get_phone_number' );

?>
