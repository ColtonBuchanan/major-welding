<?php
/*
Plugin Name: LocalSphere Custom Dev
Description: Welder custom role, custom time cards post type, and Google Calendar integration.
Version:     2.0
Author:      LocalSphere
Author URI:  http://localsphere.com
*/

require_once 'scripts.php';
require_once 'shortcodes.php';
require_once 'dashboard.php';
require_once 'admin/time-cards-admin.php';
require_once 'roles.php';
require_once 'forms/dispatch.php';
require_once 'forms/approval.php';
require_once 'forms/time-card.php';
//require_once 'forms/acf-forms.php';
require_once 'notifications.php';
//require_once 'google-api-php-client/autoload.php';
require_once 'vendor/autoload.php';
require_once 'calendar.php';
require_once 'work-order-numbers.php';


/*
* Create WorkOrders table on plugin activation
*/

register_activation_hook( __FILE__, 'ls_create_work_orders_table' );

function ls_create_work_orders_table() {

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

	global $wpdb;
	$charset_collate = $wpdb->get_charset_collate();

	$table = $wpdb->prefix . 'workorders';

	$sql = "CREATE TABLE $table (
  			  wo_number VARCHAR(9)
			) $charset_collate;";

	dbDelta( $sql );
}