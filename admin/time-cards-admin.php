<?php

/*
* CREATE CUSTOM TIME CARD STATUSES
*/

function ls_create_time_card_statuses() {

	$statuses = array(
		'order-received'     => 'Order Received',
		'order-assigned'     => 'Order Assigned',
		//'awaiting-signature' => 'Awaiting Signature',
		'order-complete'     => 'Order Complete'
	);

	foreach( $statuses as $slug => $label ) {

		register_post_status( $slug, array(
			'label'                     => _x( $label, 'post' ),
			'public'                    => true,
			'exclude_from_search'       => true,
			'show_in_admin_all_list'    => true,
			'show_in_admin_status_list' => true,
			'label_count'               => _n_noop( $label . ' <span class="count">(%s)</span>', $label . ' <span class="count">(%s)</span>' ),
		) );

	}

}

add_action( 'init', 'ls_create_time_card_statuses' );


// Modify time card statuses and fields in WP Admin

function ls_append_time_card_statuses() {

 	global $post;

 	$post_status = $post->post_status;

 	if($post->post_type == 'time-card'){

 		?>
 		<script>

 		// Change "Publish" button to "Update"

 			var btnToChange = jQuery('#publish');

 			function changeButton() {
 				jQuery(btnToChange).attr({
	 				name: 'save',
	 				value: 'Update',
	 			});
 			}

 			changeButton();

 			// Change it back to "Update" whenever status changes

 			jQuery('#post_status').change(changeButton);

 			// And change it back at a set interval

 			window.setInterval(changeButton, 250);


 		// Add electronic signature image

 		var sigContainer = jQuery('#acf-digital_signature');
 		var sig = sigContainer.find('#acf-field-digital_signature').val();

 		sigContainer.append('<img src="' + sig + '"/>');



 		// Modify post status to show custom post status

 		function lsModifyPostStatus(status) {

 			jQuery(".misc-pub-section label").append('<span id="post-status-display"> ' + status + '</span>');

 		}


 		// Modify post status dropdown

 		function lsModifyStatusDropdown(status) {

 			var postStatus = jQuery('#post_status, select[name="status"]');
      		jQuery(postStatus).append('<option value="order-received">Order Received</option>');
      		jQuery(postStatus).append('<option value="order-assigned">Order Assigned</option>');
      		//jQuery(postStatus).append('<option value="awaiting-signature">Awaiting Signature</option>');
      		jQuery(postStatus).append('<option value="order-complete">Order Complete</option>');
      		jQuery(postStatus).find('option[value="pending"]').remove();
  			jQuery(postStatus).find('option[value="draft"]').remove();
  			jQuery(postStatus).find('option[value="publish"]').remove();

  			if (typeof status !== 'undefined') {
			    jQuery(postStatus).find('option[value="' + status + '"]').attr('selected', 'selected');
			};

 		}

 		</script>
 		<?php

 		// Change correct (custom) post statuses

      	switch ($post_status) {

      		case 'order-received':

	        	?>
		      	<script>

		      		lsModifyPostStatus('Order Received');	
		      		lsModifyStatusDropdown('order-received');

		      	</script>
		      	<?php
		      	break;

		    case 'order-assigned':

		    	?>
		      	<script>

		      		lsModifyPostStatus('Order Assigned');
	      			lsModifyStatusDropdown('order-assigned');

		      	</script>
		      	<?php
		      	break;

		    case 'order-complete':

			    ?>
		      	<script>
		      		
		      		lsModifyPostStatus('Order Complete');
		      		lsModifyStatusDropdown('order-complete');

		      	</script>
		      	<?php
		      	break;

		    default:

		    	?>
		      	<script>
		      		
		      		lsModifyStatusDropdown();

		      	</script>
		      	<?php
		      	break;
		}

 	}
}

add_action( 'admin_footer-post.php', 'ls_append_time_card_statuses' );


// Change "Author" label to "Welder"

function ls_change_author_to_welder() {
	?>
	<script>
		jQuery('#authordiv .hndle span').html('Welder');
	</script>
	<?php
}

add_action( 'admin_footer-post.php', 'ls_change_author_to_welder' );



// Hide ACF status field and ACF event_id fields in WP Admin 

function ls_hide_acf_status_in_admin() {
	?>
	<script>
		jQuery('#acf-event_id').hide();
	</script>
	<?php
}

add_action( 'admin_footer-post.php', 'ls_hide_acf_status_in_admin' );


/*
* MODIFY DEFAULT AUTHOR DROPDOWN TO ONLY INCLUDE WELDERS
*/

function ls_authors_dropdown($output) {

	// Cancel the function if the user is not editing a time card

	$current_screen = get_current_screen();

	if ( $current_screen->parent_file != 'edit.php?post_type=time-card' ) {
		return $output;
	}

	// Retrieve the usernames and ID's of all welders

	$welders = get_users( array(
		'role' => 'welder',
		'fields' => array('ID', 'user_login'),
	));

	// Retrieve the ID of the currently selected welder

	global $post;

	$current_welder = $post->post_author;

	// Create the actual dropdown box

	$output = '<select name="post_author_override" id="post_author_override" class="">';

	$selected = '';

	// Create an "unassigned" option

	if( 'Unassigned' == get_the_author() ) {
		$selected = ' selected="selected"';
	}

	$output .= '<option value="Unassigned"' . $selected . '>Unassigned</option>';

	// Create an option for each welder

	foreach($welders as $welder) {

		$selected = '';

		if( $welder->ID == $current_welder ) {
			$selected = ' selected="selected"';
		}

		$output .= '<option value="' . $welder->ID . '"' . $selected . '>' . $welder->user_login . '</option>';
	}

	$output .= '</select>';

	// Return the new dropdown menu

	return $output;

}

add_filter( 'wp_dropdown_users', 'ls_authors_dropdown');

?>