<?php

function ls_add_plugin_scripts() {

	wp_enqueue_script( 'jsignature', plugins_url('localsphere-custom-dev/js/jSignature/jSignature.min.noconflict.js'), array('jquery'), false, true );

	wp_enqueue_script( 'jsignature-custom', plugins_url('localsphere-custom-dev/js/jsig-custom.js'), array('jquery', 'jsignature' ) );

	wp_enqueue_script( 'cable-charges', plugins_url('localsphere-custom-dev/js/cable-charges.js'), array('jquery') );
}

add_action( 'wp_enqueue_scripts', 'ls_add_plugin_scripts' );

?>