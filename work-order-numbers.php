<?php

/*
* Get Work Order Number from the DB and remove it to avoid duplicates
*/

function ls_take_wo_number() {

	global $wpdb;

	// Get the first available number in wp_workorders

	$result = $wpdb->get_row( "SELECT * FROM wp_workorders ORDER BY wo_number LIMIT 1", ARRAY_A );

	// Delete this number

	$wpdb->delete( 'wp_workorders', array( 'wo_number' => $result['wo_number'] ) );

	// Then return it

	return $result['wo_number'];

}