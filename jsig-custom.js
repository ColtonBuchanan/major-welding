/*
* This file uses the jQuery plugin "jSignature". 
* Here we are adding electronic signature functionality to the front-end of our Time Cards. 
* The signature is then saved to the back-end as an image URL along with the rest of the Advanced Custom Fields form data.
*/


jQuery(document).ready(function($) {

	// Cache our selectors

	var form         = jQuery('.acf-form');
	var sigContainer = form.find('#acf-digital_signature');
	var sig          = sigContainer.find('#acf-field-digital_signature');
	var newSig       = sigContainer.find('.jSignature');

	// Hide the digital signature text field. Its purpose is to hold the file path to the image once the signature is completed.
	// Customers don't need to see it.

	sig.hide();

	// If a signature already exists, show the signature as an image

	if ( sig.val() ) {

		sigContainer.append('<img src="' + sig.val() + '"/>');
		newSig.hide();

	// Otherwise create the canvas for the digital signature.

	} else {

		if ( sigContainer.is(':visible') ) {

			sigContainer.jSignature();

		}
	}

	// When the Update button is clicked, do the following:

    $('input[type="submit"]').on('click', function() {

    	// Save the canvas as an image URL

    	var img = form.jSignature('getData');

    	// Add this URL to our hidden digital signature text field. Now this reference will be saved into the back-end.

    	form.find('#acf-field-digital_signature').val(img);

    });

});