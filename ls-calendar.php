<?php

// Setup Google Calendar API

function ls_setup_calendar_service() {


  $base = plugin_dir_path(__FILE__);

  $client_email = 'calendarpluginserviceaccount@poetic-palace-123823.iam.gserviceaccount.com';
  $private_key = file_get_contents($base . 'calendarPluginServiceAccount.p12');
  $scopes = array('https://www.googleapis.com/auth/calendar');
  $user_to_impersonate = 'guy@majorwelding.com';
  $credentials = new Google_Auth_AssertionCredentials(
    $client_email,
    $scopes,
    $private_key,
    'notasecret',
    'http://oauth.net/grant_type/jwt/1.0/bearer',
    $user_to_impersonate
  );

  $config = new Google_Config();
  $config->setClassConfig('Google_Cache_File', array('directory' => $base . 'cache'));

  $client = new Google_Client($config);
  $client->setAssertionCredentials($credentials);
  if ($client->getAuth()->isAccessTokenExpired()) {
    $client->getAuth()->refreshTokenWithAssertion();
  }

  return new Google_Service_Calendar($client);

}


// Post event to calendar when status changes to Order Assigned

function ls_add_to_calendar($new_status, $old_status, $post) {

  if( $old_status == 'order-received' && $new_status == 'order-assigned' ) {

    $id                = $post->ID;
    $welder            = get_post_field ('post_author', $id);;
    $calendar          = get_the_author_meta('google_calendar_id', $welder);
    $calendar_service  = ls_setup_calendar_service();
    $title             = get_the_title($id);
    $start_date        = get_field('start_date', $id);
    $end_date          = get_field('end_date', $id);
    $location          = get_field('project_address', $id);
    $description       = get_field('work_description', $id);
    $event_id          = get_field('event_id');


    //if ( ! empty( $welder ) && ! empty( $start_date ) && ! empty( $calendar) && ! empty( $event_id ) ) {

      $event_data = new Google_Service_Calendar_Event(array(
        'summary'     => html_entity_decode($title),
        'id'          => $event_id,
        'location'    => html_entity_decode($location),
        'description' => html_entity_decode($description),
        'start'       => array(
          'date'      => $start_date,
          'timeZone'  => 'America/Vancouver'
        ),
        'end'         => array(
          'date'      => $end_date,
          'timeZone'  => 'America/Vancouver'
        )
      ));

      try {

        $event = $calendar_service->events->insert($calendar, $event_data);

      } catch ( Exception $e ) {

        echo $e->getMessage();
        die();
      }

    //}

  }
}

add_action( 'transition_post_status', 'ls_add_to_calendar', 30, 3 );




// Change event data on Google Calendar if welder modifies it from WordPress

function ls_edit_event($post_id) {

  $status = get_post_status($post_id);

  if( $status == 'order-assigned' || $status == 'awaiting-signature' ) {

    $id = $post_id;

    // Get Time Card data
    $calendar_service  = ls_setup_calendar_service();
    $welder            = get_post_field('post_author', $post_id);
    $calendar          = get_the_author_meta('google_calendar_id', $welder);
    $title             = get_the_title($id);
    $start_date        = get_field('start_date', $id);
    $end_date          = get_field('end_date', $id);
    $location          = get_field('project_address', $id);
    $description       = get_field('work_description', $id);
    $event_id          = get_field('event_id', $id);


    // Get the Event object

    try {

      $event = $calendar_service->events->get($calendar, $event_id);

    } catch(Exception $e) {

      return false;

    }

    // Update location, summary, description, etc...
    $event->setLocation(html_entity_decode($location));
    $event->setSummary(html_entity_decode($title));
    $event->setDescription(html_entity_decode($description));

    // Create new EventDateTime object for start time and update it
    $start = new Google_Service_Calendar_EventDateTime();
    $start->setDate($start_date);
    $start->setTimeZone('America/Vancouver');
    $event->setStart($start);

    // Create new EventDateTime object for end time and update it
    $end = new Google_Service_Calendar_EventDateTime();
    $end->setDate($end_date);
    $end->setTimeZone('America/Vancouver');
    $event->setEnd($end);

    // Send back the modified Event object
    if ( !empty($event) ) {
      $updated_event = $calendar_service->events->update($calendar, $event_id, $event);
    }

  }

}

add_action( 'acf/save_post', 'ls_edit_event', 10, 3 );

?>
