<?php

// Add 'View Welder Pages' capability to Admins and Welders

$admin          = get_role('administrator');
$welder         = get_role('welder');
$dispatch_admin = get_role('dispatch_admin');


$admin->add_cap('view_welder_pages');
$dispatch_admin->add_cap('view_welder_pages');
$welder->add_cap('view_welder_pages');

$welder->add_cap('read');
$welder->add_cap('edit_posts');
$welder->add_cap('edit_others_posts');
$welder->add_cap('publish_posts');
$welder->add_cap('read_private_posts');
$welder->add_cap('delete_posts');
$welder->add_cap('delete_private_posts');
$welder->add_cap('delete_published_posts');
$welder->add_cap('delete_others_posts');
$welder->add_cap('edit_private_posts');
$welder->add_cap('edit_published_posts');
$welder->add_cap('level_1');

$dispatch_admin->add_cap('read');
$dispatch_admin->add_cap('edit_posts');
$dispatch_admin->add_cap('edit_others_posts');
$dispatch_admin->add_cap('publish_posts');
$dispatch_admin->add_cap('read_private_posts');
$dispatch_admin->add_cap('delete_posts');
$dispatch_admin->add_cap('delete_private_posts');
$dispatch_admin->add_cap('delete_published_posts');
$dispatch_admin->add_cap('delete_others_posts');
$dispatch_admin->add_cap('edit_private_posts');
$dispatch_admin->add_cap('edit_published_posts');
$dispatch_admin->add_cap('level_8');
$dispatch_admin->add_cap('manage_options');
$dispatch_admin->add_cap('create_users');
$dispatch_admin->add_cap('edit_users');
$dispatch_admin->add_cap('delete_users');


// Redirect those who aren't welders when they try to view the welder calendar

function ls_redirect_welder() {
	if( is_page(434) && ! current_user_can('view_welder_pages') ) {

		wp_redirect( home_url() );
		exit;

	} else if (is_page(434) && current_user_can('view_welder_pages' ) ) {
		return;
	}
}

add_action('get_header', 'ls_redirect_welder');


/*
Redirect users to the appropriate place when they try to view their profile.
Members go to member's profile.
Welders go to welder dashboard.
*/

function ls_redirect_mw() {

	if( is_page(477) && ! current_user_can('view_welder_pages') ) {

		wp_redirect( home_url() . '/members-profile/' );
		exit;

	} else if (is_page(477) && current_user_can('view_welder_pages' ) ) {
		return;
	}
}
add_action('get_header', 'ls_redirect_mw');


/*
* PREVENT WP-ADMIN ACCESS FOR EVERYONE OTHER THAN ADMINISTRATORS AND DISPATCH ADMINS
*/

function ls_prevent_admin() {

	if ( is_admin() &&  ! current_user_can('administrator') && ! current_user_can('dispatch_admin') && ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
		wp_redirect( home_url() );
	}
}

add_action('init', 'ls_prevent_admin');

/*
WELDER REDIRECTS
Direct each welder to their own calendar page
*/

function ls_redirect_calendar() {
	$user_ID = get_current_user_id();

// Second Welder's Calendar
	if( is_page(434) && $user_ID == 22 ) {
		wp_redirect( home_url() . '/calendar2/' );
		exit;

// Guy's Calendar
	} else if (is_page(434) && $user_ID == 13 ) { 
		wp_redirect( home_url() . '/calendar_guy/' );
		exit;

// First Welder's Calendar
	} else if (is_page(434) && ! $user_ID == 22 ) {
		return;
	}
}
add_action('get_header', 'ls_redirect_calendar');

?>