<?php

/*
* REDIRECT USERS ON LOGIN
*/

add_filter( 'wpmem_login_redirect', 'ls_login_redirect', 10, 2 );

function ls_login_redirect( $redirect_to, $user_id ) {

	if ( user_can( $user_id, 'view_welder_pages' ) ) {
		return '/welders-area';
	} else {
	    return '/members-profile';
	}
}


/*
* CREATE TIME CARD CUSTOM POST TYPE
*/

function time_card_post_type() {

	// Set UI labels for time card
	$labels = array(
		'name'                => _x( 'Time Cards', 'Post Type General Name' ),
		'singular_name'       => _x( 'Time Card', 'Post Type Singular Name' ),
		'menu_name'           => __( 'Time Cards' ),
		'parent_item_colon'   => __( 'Parent Time Card' ),
		'all_items'           => __( 'All Time Cards' ),
		'view_item'           => __( 'View Time Card' ),
		'add_new_item'        => __( 'Add New Time Card' ),
		'add_new'             => __( 'Add New' ),
		'edit_item'           => __( 'Edit Time Card' ),
		'update_item'         => __( 'Update Time Card' ),
		'search_items'        => __( 'Search Time Card' ),
		'not_found'           => __( 'Not Found' ),
		'not_found_in_trash'  => __( 'Not Found in Trash' ),
	);
	
	// Set other options for time card
	$args = array(
		'label'               => __( 'Time Cards' ),
		'description'         => __( 'Your Time Cards' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'author', 'revisions', 'custom-fields' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'rewrite'             => array( 'slug' => 'time-cards' ),
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	
	// Register our time card post type
	register_post_type( 'time-card', $args );

}

add_action( 'init', 'time_card_post_type' );



/*
* MODIFY THE TIMECARD ARCHIVE TO ONLY SHOW TIMECARDS THAT BELONG TO THE CURRENT USER
*/

// function ls_modify_timecard_query($query) {

// 	if( $query->is_main_query() && is_post_type_archive('time-card') && ! is_admin() ) {
// 		$query->set( 'author', get_current_user_ID() );
// 		$query->set( 'post_status', array('order-assigned', 'awaiting-signature', 'publish') );
// 		return;
// 	}
// }

// add_action( 'pre_get_posts', 'ls_modify_timecard_query' );



/*
* DISPLAY THE REMAINING DATA DEPENDING ON USER TYPE
*/

function ls_display_order_details( $post_status ) {

	$ignore = array();

	// Determine if user is a welder or admin

	$welder = current_user_can('view_welder_pages');


	// Get all ACF data associated with the time card

	$fields = get_fields();

	
	// Create the table for outputting data

	if ( $fields ) {

		if ( $post_status !== 'order-complete' ) {

			echo '<a class="order-details-btn-link ' . $post_status . '" id="show_order_details" href="#">';

				echo '<div class="order-details-btn"><h3>Click for More Details <i class="fa fa-angle-down"></i></h3></div>';

			echo '</a>';

		}

		echo '<table class="ls-table ls-order-details ' . $post_status . '" id="details_to_show"><tbody>';
	}


	// Display the appropriate data

	foreach( $fields as $field_name => $value ) {

		$field = get_field_object($field_name, false, array('load_value' => false));
		
		if ( $welder ) {

			// If user is a welder/admin and order is complete, ignore items in array

			if ( $post_status == 'order-complete' ) {

				$ignore = array(
					'event_id',
					'end_date',
					'work_description',
					'billable_hours',
					'is_work_completed',
				);

			// If user is a welder/admin and order is not complete, ignore these other items in array

			} else {

				$ignore = array(
					'digital_signature',
					'date_signed',
					'event_id',
					'end_date',
					'work_description',
					'billable_hours',
					'is_work_completed',
				);
			}

			// Display the remaining items

			if ( in_array( $field_name, $ignore ) ) {

				continue;

			} else {

				if ( $field_name == 'digital_signature' ) {
					$value = '<img src="' . $value . '"/>';
				}

				echo '<tr><td>' . $field['label'] . '</td><td>' . $value . '</td></tr>';
			}

		} else {

			// If it's a customer, and the order is complete, ignore items in array

			if ( $post_status == 'order-complete' ) {

				$ignore = array(
					'event_id',
				);

			// If it's a customer, and the order hasn't been signed, ignore these other items in array

			} else {

				$ignore = array(
					'digital_signature',
					'date_signed',
					'event_id'
				);

			}	 

			// Display the remaining items

			if ( in_array( $field_name, $ignore ) ) {

				continue;
				
			} else {

				if ( $field_name == 'digital_signature' ) {
					$value = '<img src="' . $value . '"/>';
				}

				echo '<tr><td>' . $field['label'] . '</td><td>' . $value . '</td></tr>';
			}

		}

	}

	// Close the table (if there is one)

	if ( $fields ) {
		echo '</tbody></table>';
	}

}


function ls_display_dispatch_details() {

	$dispatch_fields = array(
		array( 'Company', get_field('company') ),
		array( 'Billing Address', get_field('billing_address') ),
		array( 'Customer Name', get_field('customer_name') ),
		array( 'Customer\'s Job Title', get_field('customers_job_title') ),
		array( 'Customer Email', get_field('customer_email') ),
		array( 'Customer Phone NUmber', get_field('customer_phone_number') ),
		array( 'Site Address', get_field('site_address') ),
		array( 'Site Contact Name', get_field('site_contact_name') ),
		array( 'Site Contact Phone Number', get_field('site_contact_phone_number') ),
		array( 'Start Date', get_field('start_date') ),
		array( 'End Date', get_field('end_date') ),
		array( 'Customer PO#', get_field('customer_po') ),
		array( 'Type of Welding', get_field('type_of_welding') ),
		array( 'Governing Code', get_field('governing_code') ),
		array( 'Inspection Types', get_field('inspection_types') ),
		array( 'Material Type', get_field('material_type') ),
		array( 'Process', get_field('process') ),
		array( 'Buzz Box Required', get_field('buzz_box_required') ),
		array( 'Lead Cable Required', get_field('lead_cable_required') ),
		array( 'Ground Cable Required', get_field('ground_cable_required') ),
		array( 'Safety Certificates Required', get_field('safety_certificates_required') ),
		array( 'Rig Welders Required', get_field('rig_welders_required') ) 
	);

	if ( $post_status !== 'order-complete' ) {

		echo '<a class="order-details-btn-link ' . $post_status . '" id="show_order_details" href="#">';

			echo '<div class="order-details-btn"><h3>Project Details <i class="fa fa-angle-down"></i></h3></div>';

		echo '</a>';

	}

	echo '<table class="ls-table ls-order-details ' . $post_status . '" id="details_to_show"><tbody>';

	foreach ( $dispatch_fields as $dispatch_field ) {
		echo '<tr><td>' . $dispatch_field[0] . '</td><td>' . $dispatch_field[1] . '</td></tr>';
	}

	echo '</tbody></table>';
}


function ls_display_time_cards() {

	$time_cards = get_field('time_cards');

	if ( !empty( $time_cards ) ) {

		echo '<div class="time-cards">';

			echo '<h2>Submitted Time Cards</h2>';

			foreach ( $time_cards as $key => $time_card ) {

				if ( $time_card['is_work_completed'] ) {
					$is_work_completed = 'Yes';
					$return_date = 'N/A';
				} else {
					$is_work_completed = 'No';
					$return_date = $time_card['return_date'];
				}

				?>

				<article class="time-card">
					<table class="ls-table">
						<tbody>
							<tr>
								<th>Welder</th>
								<th>Date</th>
								<th>Billable Hours</th>
								<th>Start Time</th>
								<th>End Time</th>
							</tr>
							<tr>
								<td><?php echo $time_card['welder']; ?></td>
								<td><?php echo $time_card['date']; ?></td>
								<td><?php echo $time_card['billable_hours']; ?></td>
								<td><?php echo $time_card['start_time']; ?></td>
								<td><?php echo $time_card['end_time']; ?></td>
							</tr>
						</tbody>
					</table>

					<table class="ls-table">
						<tbody>
							<tr>
								<th>Work Description</th>
								<th>Is Work Completed?</th>
								<th>Return Date</th>
							</tr>
							<tr>
								<td><?php echo $time_card['work_description']; ?></td>
								<td><?php echo $is_work_completed; ?></td>
								<td><?php echo $return_date; ?></td>
							</tr>
						</tbody>
					</table>

					<table class="ls-table">
						<tbody>
							<tr>
								<th>Site Contact</th>
								<th>Site Contact Signature</th>
							</tr>
							<tr>
								<td><?php echo $time_card['site_contact']; ?></td>
								<td><?php echo '<img src="' . $time_card['site_signature'] . '">'; ?></td>
							</tr>
						</tbody>
					</table>
				

					<?php

				echo '</article>';
			}

		echo '</div>';

		update_field('time_cards', $time_cards, get_the_ID());

	}

}


function ls_check_if_work_is_completed( $time_cards ) {

	// Make it false by default

	$is_work_completed = false;

	// Check the "Is Work Completed" field in each time card for a true value.
	// If a true value is found, then work is completed.

	foreach( $time_cards as $time_card ) {

		if ( $time_card['field_56fd5261f7831'] == 1 ) {

			$is_work_completed = true;
		}
	}

	// Return true or false

	return $is_work_completed;

}


?>